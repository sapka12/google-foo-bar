import unittest
import solution


class Test(unittest.TestCase):

    def test_3(self):
        self.assertEqual(1, solution.solution(3))

    def test_4(self):
        self.assertEqual(1, solution.solution(4))

    def test_5(self):
        self.assertEqual(2, solution.solution(5))

    def test_15(self):
        self.assertEqual(26, solution.solution(15))

    def test_10(self):
        self.assertEqual(9, solution.solution(10))

    def test_48(self):
        self.assertEqual(2909, solution.solution(48))

    def test_49(self):
        self.assertEqual(3263, solution.solution(49))

    def test_50(self):
        self.assertEqual(3657, solution.solution(50))

    # def test_200(self):
    #     self.assertEqual(487067745, solution.solution(200))


if __name__ == '__main__':
    unittest.main()
