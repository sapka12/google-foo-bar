import itertools
import time


def flatten(xss):
    return itertools.chain.from_iterable(xss)


def inc_element(state, idx):
    new_state = state.copy()
    if idx == len(state):
        new_state.append(1)
    else:
        new_state[idx] = new_state[idx] + 1
    return new_state


def is_valid(arr):
    return sorted(arr, reverse=True) == arr and len(arr) == len(set(arr))


def calculate_next_options(state):
    return filter(
        lambda a: is_valid(a),
        map(
            lambda idx: inc_element(state, idx),
            range(len(state) + 1)
        )
    )


def unique_list(list_of_lists):
    return [list(x) for x in set(tuple(x) for x in list_of_lists)]


def options(n):
    if n == 3:
        return [[2, 1]]
    previous_options = options(n - 1)
    print(n - 1, 'calculated: ', len(previous_options), time.time())
    print(n - 1, previous_options)
    print('-----------------------------')

    return unique_list(flatten([calculate_next_options(p) for p in previous_options]))


def solution(n):
    return len(options(n))


# def solution(n):
#     coefficients = [1] + [0] * n
#     for i in range(1, n + 1):
#         for j in range(n, i - 1, -1):
#             coefficients[j] += coefficients[j - i]
#         # print(i, coefficients)
#     return coefficients[n] - 1


if __name__ == '__main__':
    solution(200)
