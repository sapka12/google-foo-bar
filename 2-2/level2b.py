# Power Hungry
# ============
#
# Commander Lambda's space station is HUGE. And huge space stations take a LOT of power.
# Huge space stations with doomsday devices take even more power.
# To help meet the station's power needs, Commander Lambda has installed solar panels on the station's
# outer surface. But the station sits in the middle of a quasar quantum flux field, which wreaks
# havoc on the solar panels. You and your team of henchmen have been assigned to repair the solar
# panels, but you'd rather not take down all of the panels at once if you can help it, since they do
# help power the space station and all!
#
# You need to figure out which sets of panels in any given array you can take offline to repair
# while still maintaining the maximum amount of power output per array, and to do THAT, you'll
# first need to figure out what the maximum output of each array actually is. Write a function
# solution(xs) that takes a list of integers representing the power output levels of each panel
# in an array, and returns the maximum product of some non-empty subset of those numbers. So for
# example, if an array contained panels with power output levels of [2, -3, 1, 0, -5], then the
# maximum product would be found by taking the subset: xs[0] = 2, xs[1] = -3, xs[4] = -5, giving
# the product 2*(-3)*(-5) = 30.  So solution([2,-3,1,0,-5]) will be "30".
#
# Each array of solar panels contains at least 1 and no more than 50 panels, and each panel will
# have a power output level whose absolute value is no greater than 1000 (some panels are malfunctioning
# so badly that they're draining energy, but you know a trick with the panels' wave stabilizer that
# lets you combine two negative-output panels to produce the positive output of the multiple of their
# power values). The final products may be very large, so give the solution as a string representation
# of the number.

def solution(xs):
    from functools import reduce

    if len(xs) == 1:
        return str(xs[0])

    positive = list(filter(lambda a: a > 0, xs))
    negative = list(filter(lambda a: a < 0, xs))
    if len(negative) % 2 == 1:
        negative = list(sorted(negative, reverse=True)[1:])

    all_nums = positive + negative
    return str(abs(0 if not all_nums else reduce(lambda a, b: a * b, all_nums, 1)))


if __name__ == '__main__':
    def must_eq(current, expected):
        print('current', current, 'expected', expected)
        assert current == expected, current


    must_eq(solution([2, -3, 1, 0, -5]), "30")
    must_eq(solution([2, 0, 2, 2, 0]), "8")
    must_eq(solution([-2, -3, 4, -5]), "60")

    must_eq(solution([-2, 3, 4, -5]), "120")
    must_eq(solution([2, 2, 2, 2, 2]), "32")
    must_eq(solution([-2, -2, -2, -2]), "16")
    must_eq(solution([0]), "0")
    must_eq(solution([1]), "1")
    must_eq(solution([-1]), "-1")
    must_eq(solution([]), "0")
    must_eq(solution([0, 1]), "1")
    must_eq(solution([0, 0]), "0")
    must_eq(solution([1000, 1000]), "1000000")
    must_eq(solution(list([1000 for i in range(2)])), "1000000")
    must_eq(solution(list([1000 for i in range(50)])), str(1000 ** 50))
    must_eq(solution(list([-1000 for i in range(50)])), str(1000 ** 50))
    must_eq(solution(list([-1000 for i in range(51)])), str(1000 ** 50))

    assert str(1000 ** 50).count('0') == 3 * 50
